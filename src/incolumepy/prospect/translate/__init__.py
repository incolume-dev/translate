import toml
import pathlib

__package__ = pathlib.Path(__file__).parent.parent.parent.parent.parent
__version__ = toml.load(__package__/'pyproject.toml')['tool']['poetry']['version']

